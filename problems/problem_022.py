# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"


# check if day is not sunny and is a workday

# return a list containing umbrella
# check if it is a workday

# list must contain laptop

# if not a workday
# list must contain surfboard


def gear_for_day(is_workday, is_sunny):
    needs = []
    if is_sunny == False:
        needs.append('umbrella')
    if is_workday == True:
        needs.append('laptop')
    elif is_workday == False:
        needs.append('surfboard')
        return needs


gear_for_day(True, False)
