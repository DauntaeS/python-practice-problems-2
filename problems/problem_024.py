# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# remember count of values list
# add all numbers in list together
# return the average by dividing the added amount by the length of list



def calculate_average(values):
    if len(values) == 0:
        return None
    else:
        count = 0
        for num in values:
            count = num + count
    divide = count / len(values)
    print(int(divide))


calculate_average([2,3,3,5,7,10])
