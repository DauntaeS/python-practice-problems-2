# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# compare if word is the same forward and backward
# if true return yes
# else no
# how would you compare them?
# you may need to store each letter first
# break the problem down to its smallest form

# assign reverse to an empy variable
# then join the reversed word with ''
# then compare them


def is_palindrome(word):
    rev = reversed(word)
    reversed1 = ''.join(rev)
    if word == reversed1:
        print(True)
    else:
        print(False)


is_palindrome("Hello")
is_palindrome("racecar")
