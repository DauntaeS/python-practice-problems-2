# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

# iterate over each letter in a string
# if there are two letters that are the same
# remove those letters and return a string

def remove_duplicate_letters(s):
    if len(s) == 0:
        return None
    else:
        copy = sorted(set(s))
        return copy


remove_duplicate_letters("bccbada")
