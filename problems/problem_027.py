# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

# loop over a list and return the max num

def max_in_list(values):
    # return max(values)
    if len(values) == 0:
        return None
    else:
        return max(values)


max_in_list([5,10,20, 1000])
