# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# iterate over a list of numbers
# and compare each number
# then what ever number is the 2nd to largest return that number
# if the list is empty return None


def find_second_largest(values):
    if len(values) <= 1:
        return None
    else:
        new_list = set(values)
        print(new_list.remove(max(values)))



find_second_largest([5,15,10,20])
