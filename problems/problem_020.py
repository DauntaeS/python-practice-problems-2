# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

# get the length of attendees_list
# get the length of members_list and multipy by 0.5 to get 50%
# compare if length of attendees_list >= members_list halved
# return


def has_quorum(attendees_list, members_list):
    halved = len(members_list) * 0.5
    if len(attendees_list) >= halved:
        return True
    else:
        return False



has_quorum(["james", "jackie", "jimma", "tammy"], ["mark", "cam", "sam", "nick", "dick", "rick"])
