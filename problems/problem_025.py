# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

# check if list is empty if so return None
# otherwise add all of the values together

def calculate_sum(values):
    if len(values) == 0:
        return None
    else:
        print(sum(values))

calculate_sum([2,3,3,5,7,10])
