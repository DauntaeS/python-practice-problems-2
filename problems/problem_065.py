# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(nums):
    # takes the value in index 1 and subtracts it by index 0 and returns the abs() value which is 2 in this case
    max_gap = abs(nums[1] - nums[0])
    # loops through the nums list from the first index up to and including the last index
    for i in range(1, len(nums) - 1):
        # this next part was a bit tricky to understand but whatever index it currently is at it adds 1 so its grabbing the next index so currently its 3 but because your adding 1 it moves to the next index which is 5
        # then it subtracts that index by the current index without the plus 1 in this case 5 - 3
        gap = abs(nums[i + 1] - nums[i])
        # then checks if the gap is greater than the max_gap & if so it replaces the max_gap with the greater number held by gap
        if gap > max_gap:
            # then it returns max_gap being the larger number which is 2 in this case
            max_gap = gap
    return max_gap




print(biggest_gap([1,3,5,7]))
